# Small project

# Projekt Generátor Statických Stránek

Tento projekt používá MkDocs pro generování statických HTML stránek z Markdown souborů. CI/CD pipeline na GitLabu se stará o automatické generování a nasazení těchto stránek.

## Prerekvizity

- Git
- MkDocs

## Instalace

1. Klonujte tento repozitář:
   ```sh
   git clone git@gitlab.com:Madas42/small-project.git
   cd small-project

## CI/CD Pipeline

CI/CD pipeline na GitLabu se skládá ze tří fází:

1. **Test**: V této fázi se kontroluje kvalita Markdown souborů pomocí nástroje `markdownlint`.
2. **Build**: V této fázi se generují statické HTML stránky z Markdown souborů pomocí `mkdocs`.
3. **Deploy**: V této fázi se výsledné stránky nasazují na GitLab Pages.

## Konfigurace

Tento projekt používá konfigurační soubor `mkdocs.yml`, který definuje několik aspektů webu, včetně názvu webu, autora, popisu, copyrightu, repozitáře, tématu, pluginů a rozšíření Markdownu.
Také definuje strukturu stránek.